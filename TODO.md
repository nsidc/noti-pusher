=======
noti-pusher
===========


## TODO list

Change hard coded variables to var from the config.yml file
Modify the /message POST endpoint to handle JSON messages
Add channel mechanism to handle multiple apps in the same port
Modify /message to handle targeted messaging i.e. just to an specific app
Improve the demonization and deployment
Add more GET endpoints to collect real time data from the applications