require File.join(File.dirname(__FILE__), '..', 'config', 'config')

require 'em-websocket'
require 'sinatra/base'
require 'thin'
require 'json'

# NotiPusher sends authenticated HTTP post messages to client applications
# suscribed to NotiPusher.channel
# NotiPusher.channel must be an accessible URL:PORT URL for the clients
module NotiPusher

  def self.em_connections
    @em_connections ||= []
  end

  # Simple Sinatra app that handles the REST endpoints
  class App < Sinatra::Base
    # threaded - False: Will take requests on the reactor thread
    #            True:  Will queue request for background thread
    configure do
      set :threaded, false
    end

    use Rack::Auth::Basic, 'Restricted Area' do |username, password|
      username == ENVIRONMENT[:user] && password == ENVIRONMENT[:password]
    end

    def notify_clients(message)
      NotiPusher.em_connections.each { |connection| connection.send(message.to_s) }
      if NotiPusher.em_connections.size > 0
        message_response = "Message posted to #{NotiPusher.em_connections.size.to_s} connected clients"
      else
        message_response = 'No active connections at the moment'
      end
      message_response
    end

    post '/message' do
      message = JSON.parse(request.env['rack.input'].read)
      notify_clients(message)
    end

    get '/status' do
      content_type :json
      { clients: NotiPusher.em_connections.size.to_s }.to_json
    end

  end

end