require 'nsidc/config_injector_client'

config_client = Nsidc::ConfigInjectorClient.new(
  'http://integration.config-injector-vm.apps.int.nsidc.org:10680'
)

ENVIRONMENT = {
  host: '127.0.0.1',
  port: '11981',
  server: 'thin',
  user: 'notifier',
  password: config_client.get('noti-pusher.password'),
  websocket: '11980'
}
