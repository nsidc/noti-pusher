noti-pusher
===========

NotiPusher is an API that allows you to send authenticated HTTP messages to your
javascript client applications via websockets.

## Requirements

* Ruby 1.9.3
* Bundler 1.2+

## Installation

```bash
git clone git@bitbucket.org:nsidc/noti-pusher.git
bundle install
```

## Quick Start Guide

 `rake start`

It will start the service using predefined host:port from config.rb, to override
it you can pass ENV variables.

 * `rake start PUSHER_HOST=127.0.0.1 PUSHER_REST_PORT=8181 PUSHER_WS_PORT=9999`

	* `PUSHER_HOST: IP on which the services will run.`
	* `PUSHER_REST_PORT: The port on which the messages are sent via HTTP post.`
	* `PUSHER_WS_PORT: The websocket port on wich the clients are suscribed to.`

`rake stop`

### REST endpoints

* HOST:PORT/message
	* handles authenticated JSON POST requests that are route to all active
      websocket connections
* HOST:PORT/status
	* handles authenticated GET requests to get the number of active websocket
      connections

### Testing

Once that the service is running we can use CURL to connect to the WS port:

```bash
curl -i -N -H "Connection: Upgrade" -H "Upgrade: websocket" -H "Host:\
        localhost" -H "Origin: http://localhost" http://127.0.0.1:9999
```

You can repeat this in 2 different terminal windows to have more than one active
connection.

Now in a different terminal you can send a message to the websocket connections
via the HTTP endpoint

```bash
curl -v --user $user:$password -H "Content-Type: application/json" -XPOST -d\
        '{"app":"test","message":"Hi there"}' http://127.0.0.1:8181/message
```

You should see the message in your websocket terminals.

### How noti-pusher is used at NSIDC

The notification service was designed to alert users that new code has been
deployed and they need to refresh the page in order to be synchronized. The
Acadis portal creates a websocket connection to the noti-pusher instance on
production and listens for incomming messages. When a new messages arrives it is
rendered in a notification area. The current instances of the niotification
service are running on 11900 for the websockets and 11980 for the admin
port. The websocket port is proxied to /api/notification.

This project can be used not just to deliver notifications but to transport any
kind of content in real time to a client application, including data or even
code.

## Version History

* 0.1.0
  * Initial release

## Versioning

This gem follows the principles of
[Semantic Versioning 2.0.0](http://semver.org/)

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## How to contact NSIDC ##

User Services and general information:  
Support: http://support.nsidc.org  
Email: nsidc@nsidc.org

Phone: +1 303.492.6199  
Fax: +1 303.492.2468

Mailing address:  
National Snow and Ice Data Center  
CIRES, 449 UCB  
University of Colorado  
Boulder, CO 80309-0449 USA

## License

Notification Push is licensed under the MIT license. See [LICENSE](LICENSE.md).
