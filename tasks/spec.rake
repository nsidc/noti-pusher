require 'rspec/core/rake_task'
require 'rubocop/rake_task'
require 'bundler/setup'

Bundler.require(:default, :test)

namespace :spec do
  RSpec::Core::RakeTask.new(:unit) do |t|
    t.pattern = './spec/**/*_spec.rb'
  end
end