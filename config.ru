require 'bundler'
require File.join(File.dirname(__FILE__), 'lib', 'noti_pusher')
require File.join(File.dirname(__FILE__), 'config', 'config')

Bundler.require :default

# Runs the sinatra app inside the EventMachine non blocking loop
def run(opts)

  # Start he reactor
  EM.run do

    server  = opts[:server] || ENVIRONMENT[:server]
    host    = opts[:host]   || ENVIRONMENT[:host]
    port    = opts[:port]   || ENVIRONMENT[:port]
    ws_port = opts[:ws_port]   || ENVIRONMENT[:websocket]
    web_app = opts[:app]

    dispatch = Rack::Builder.app do
      map '/api/notification' do
        run web_app
      end
    end

    unless ['thin', 'hatetepe', 'goliath'].include? server
      raise "Need an EM webserver, but #{server} isn't"
    end

    # Start the web server
    Rack::Server.start({
      app:    dispatch,
      server: server,
      Host:   host,
      Port:   port
    })

    Signal.trap("INT")  { EventMachine.stop }
    Signal.trap("TERM") { EventMachine.stop }

    EM::WebSocket.run(host: host, port: ws_port) do |ws|
      ws.onopen { |handshake|
        port, ip = Socket.unpack_sockaddr_in(ws.get_peername)
        puts "New client connected at #{ip}"
        NotiPusher.em_connections.push(ws)
      }

      ws.onclose {
        puts "A client just got disconnected"
        NotiPusher.em_connections.delete(ws)
      }

      ws.onmessage { |msg|
        puts "Recieved message: #{msg}"
        ws.send "Pong: #{msg}"
      }
    end

  end
end

run app: NotiPusher::App, host: ENV['PUSHER_HOST'], port: ENV['PUSHER_REST_PORT'], ws_port: ENV['PUSHER_WS_PORT']

# hack to start the service using thin/rackup with eventmachine
map '/' do
 Kernel.exit(0)
end