require File.join(File.dirname(__FILE__), '..', 'config', 'config')
ENV['RACK_ENV'] = 'test'

require 'noti_pusher'
require 'rspec'
require 'rack/test'
require 'json'

describe 'NotiPusher Sinatra App can handle REST calls' do
  include Rack::Test::Methods

  def app
    NotiPusher::App.new
  end

  it 'does not let me access the posting endpoint if we are not authorized' do
    post '/message'
    last_response.status.should == 401
  end

  it 'tells us if there are no clients' do
    authorize ENVIRONMENT[:user], ENVIRONMENT[:password]
    h = { 'Content-Type' => 'application/json' }
    body = { key: 'abcdef' }.to_json
    post '/message', body, h
    last_response.should be_ok
    last_response.body.should == 'No active connections at the moment'
  end

  it 'we get the number of active clients' do
    authorize ENVIRONMENT[:user], ENVIRONMENT[:password]
    get '/status'
    last_response.should be_ok
    last_response.body.should == '{"clients":"0"}'
  end

  # TODO: fix this and find a way to mock an EM server
  # it 'delivers the message to the active clients' do
  #   EM::Connection <- mock the connection in the NotiPusher module
  #   authorize 'admin', 'admin'
  #   h = { 'Content-Type' => 'application/json' }
  #   body = { message: 'hi there' }.to_json
  #   post '/message', body, h
  #   last_response.should be_ok
  #   last_response.body.should == 'Message posted to 1 connected clients'
  # end

end